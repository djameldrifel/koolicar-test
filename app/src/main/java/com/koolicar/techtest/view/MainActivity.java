package com.koolicar.techtest.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.koolicar.techtest.MVP;
import com.koolicar.techtest.R;
import com.koolicar.techtest.common.GenericMVPActivity;
import com.koolicar.techtest.model.venues.Venue;
import com.koolicar.techtest.presenter.MainPresenter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends GenericMVPActivity<MVP.RequiredViewOps, MVP.ProvidedPresenterOps, MainPresenter>
        implements MVP.RequiredViewOps {

    private static final String TAG = "TechTestMainActivity";

    private MainActivityAdapter mRecyclerViewAdapter;

    private final View.OnClickListener mVenueDetailClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Venue venue = (Venue) view.getTag();
            getPresenter().downloadVenuePhoto(venue);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.onCreate(MainPresenter.class, this);
        setContentView(R.layout.activity_main);
        setTitle("Test Koolicar");
        getPresenter().fetchVenueList();

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));
        mRecyclerViewAdapter = new MainActivityAdapter(this);
        recyclerView.setAdapter(mRecyclerViewAdapter);
        startDetectRootViewChanges(recyclerView);
    }

    private void startDetailActivity(Venue venue) {
        Intent intent = new Intent(MainActivity.this, VenueDetailActivity.class);
        intent.putExtras(VenueDetailActivity.addToBundle(new Bundle(), venue));
        startActivity(intent);
    }

    @Override
    public void displayVenueList(List<Venue> venueList) {
        mRecyclerViewAdapter.setItemList(venueList);
    }

    @Override
    public void displayVenueDetail(Venue venue) {
        startDetailActivity(venue);
    }

    private class MainActivityAdapter extends RecyclerView.Adapter<VenueItemViewHolder> {

        private final LayoutInflater mLayoutInflater;
        private List<Venue> mItemList = new ArrayList<>();

        public MainActivityAdapter(Context context) {
            super();
            mLayoutInflater = LayoutInflater.from(context);
        }

        @Override
        public VenueItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mLayoutInflater.inflate(R.layout.venue_item, parent, false);
            return new VenueItemViewHolder(view);
        }

        @Override
        public void onBindViewHolder(VenueItemViewHolder holder, int position) {
            Venue venue = mItemList.get(position);
            holder.mVenueName.setText(venue.getName());
            holder.mVenueCategory.setText(venue.getCategoriesString());
            holder.mVenueAdress.setText(venue.getLocation().getAddress());
            holder.itemView.setTag(venue);
            holder.itemView.setOnClickListener(mVenueDetailClickListener);
            holder.itemView.setClickable(true);
        }

        @Override
        public int getItemCount() {
            return mItemList.size();
        }

        public void setItemList(List<Venue> list) {
            mItemList = list;
            notifyDataSetChanged();
        }
    }

    private class VenueItemViewHolder extends RecyclerView.ViewHolder {
        final TextView mVenueName;
        final TextView mVenueCategory;
        final TextView mVenueAdress;

        public VenueItemViewHolder(View view) {
            super(view);
            mVenueName = (TextView) view.findViewById(R.id.venue_item_name);
            mVenueCategory = (TextView) view.findViewById(R.id.venue_item_category);
            mVenueAdress = (TextView) view.findViewById(R.id.venue_item_adress);
        }
    }
}
