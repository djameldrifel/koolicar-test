package com.koolicar.techtest.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.koolicar.techtest.R;
import com.koolicar.techtest.model.venues.Venue;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import retrofit2.converter.gson.GsonConverterFactory;

public class VenueDetailActivity extends AppCompatActivity {

    private static final String KEY_VENUE_ITEM = "venue_item";

    public static Bundle addToBundle(@NonNull Bundle bundle, Venue venue) {
        bundle.putSerializable(KEY_VENUE_ITEM, venue);
        return bundle;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.venue_detail);
        // Just using default toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Venue venue = (Venue) bundle.get(KEY_VENUE_ITEM);
            TextView venueName = (TextView) findViewById(R.id.venue_detail_name);
            venueName.setText(venue.getName());
            TextView venueCategory = (TextView) findViewById(R.id.venue_detail_category);
            venueCategory.setText(venue.getCategoriesString());
            TextView venueAdress = (TextView) findViewById(R.id.venue_detail_adress);
            venueAdress.setText(venue.getLocation().getAddress());
            TextView venueContact = (TextView) findViewById(R.id.venue_detail_contact);
            venueContact.setText(venue.getContact().getFormattedPhone());

            ImageView imageView = (ImageView) findViewById(R.id.venue_detail_image_view);
            if (venue.getImageURL() != null)
                UrlImageViewHelper.setUrlDrawable(imageView, venue.getImageURL());

            setTitle(venue.getName());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
