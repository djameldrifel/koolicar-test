package com.koolicar.techtest.api;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by djameldrifel on 2016-12-09.
 */

public class ApiManager {

    private static final String TAG = "TechTestApiManager";

    private final static String CLIENT_ID = "PQZD4JF0M0ZILLKXS1ZI3EKPDLGHY2EBSZDULMX4DKORUYLC";
    private final static String CLIENT_SECRET = "Y3ODA1PAFXWUUFWESURYK43CJCKY33VI2QCSXMHGRXP31ZNK";
    private final static String VERSION_DATE = "20161201";

    private final static String API_URL = "https://api.foursquare.com/v2/";

    private static Retrofit retrofit;

    /**/

    public static Retrofit getClient() {
        if (retrofit == null) {
            OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
            httpClientBuilder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    HttpUrl originalHttpUrl = original.url();

                    HttpUrl url = originalHttpUrl.newBuilder()
                            .addQueryParameter("client_id", CLIENT_ID)
                            .addQueryParameter("client_secret", CLIENT_SECRET)
                            .addQueryParameter("v", VERSION_DATE)
                            .build();

                    // Request customization: add request headers
                    Request.Builder requestBuilder = original.newBuilder()
                            .url(url);
                    Log.d(TAG, url.toString());
                    Request request = requestBuilder.build();
                    Response response = chain.proceed(request);
                    String jsonString = response.body().string();
                    try {
                        //To minimise number of models in the I'm filtering the result json get list of venues only
                        JSONObject jsonObject = new JSONObject(jsonString);
                        if (jsonObject.has("response") ) {
                            JSONObject responseJson = jsonObject.getJSONObject("response");
                            String parsedResponse = jsonString;
                            if(responseJson.has("venues"))
                                parsedResponse = responseJson.getJSONArray("venues").toString();
                                //Log.d(TAG,parsedResponse);
                                response = response.newBuilder()
                                        .body(ResponseBody.create(response.body().contentType(), parsedResponse)).build();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    return response;
                }
            });

            retrofit = new Retrofit.Builder()
                    .baseUrl(API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClientBuilder.build())
                    .build();
        }
        return retrofit;
    }
}
