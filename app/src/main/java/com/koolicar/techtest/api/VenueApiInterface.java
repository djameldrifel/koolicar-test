package com.koolicar.techtest.api;

import com.koolicar.techtest.model.venues.Venue;
import com.koolicar.techtest.model.venues.photo.Result;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by djameldrifel on 2016-12-09.
 */

public interface VenueApiInterface {

    int SEARCH_LIMIT = 10;

    @GET("venues/search")
    Call<List<Venue>> search(@QueryMap Map<String, String> options);

    @GET("venues/{venue_id}/photos")
    Call<Result> getPhotoUrl(@Path("venue_id") String id);
}
