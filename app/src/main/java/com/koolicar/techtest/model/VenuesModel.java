package com.koolicar.techtest.model;

import android.util.Log;

import com.koolicar.techtest.MVP;
import com.koolicar.techtest.api.ApiManager;
import com.koolicar.techtest.api.VenueApiInterface;
import com.koolicar.techtest.common.GenericModel;
import com.koolicar.techtest.model.venues.Venue;
import com.koolicar.techtest.model.venues.photo.Item;
import com.koolicar.techtest.model.venues.photo.Result;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by djameldrifel on 2016-12-10.
 */

public class VenuesModel extends GenericModel<MVP.RequiredPresenterOps> implements MVP.ProvidedModelOps {

    private static final String TAG = "TechTestVenuesModel";
    private VenueApiInterface mVenueApiInterface = ApiManager.getClient().create(VenueApiInterface.class);

    @Override
    public void downloadVenuesList() {

        Map<String, String> queryParameters = new HashMap<>();
        queryParameters.put("limit", Integer.toString(VenueApiInterface.SEARCH_LIMIT));
        queryParameters.put("ll", "45.533368,-73.621122"); //hardcoded for test purpose
        queryParameters.put("query", "food");
        Call<List<Venue>> call = mVenueApiInterface.search(queryParameters);
        //API call doesn't contain any option to sort the places with distance so can
        //so we can proceed to implement a comparable on models POJO
        call.enqueue(new Callback<List<Venue>>() {
            @Override
            public void onResponse(Call<List<Venue>> call, Response<List<Venue>> response) {
                List<Venue> venueList = response.body();
                getPresenter().onListDownloaded(venueList);
            }

            @Override
            public void onFailure(Call<List<Venue>> call, Throwable t) {
                t.printStackTrace();
                Log.e(TAG, "On failure " + t.getMessage());
            }
        });
    }

    @Override
    public void downloadVenueImage(final Venue venue) {

        Call<Result> call = mVenueApiInterface.getPhotoUrl(venue.getId());
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Result result = response.body();
                if (result.getResponse().getPhotos().getCount() > 0) {
                    Item item = result.getResponse().getPhotos().getItems().get(0);
                    venue.setImageURL(item.getPrefix() + item.getWidth() + "x" + item.getHeight() + item.getSuffix());
                }
                getPresenter().onVenuePhotoDownloaded(venue);
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                t.printStackTrace();
                Log.e(TAG, "On failure " + t.getMessage());
            }
        });
    }


}
