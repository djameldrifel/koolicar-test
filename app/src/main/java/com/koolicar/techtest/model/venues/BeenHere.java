
package com.koolicar.techtest.model.venues;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BeenHere implements Serializable {

    @SerializedName("lastCheckinExpiredAt")
    @Expose
    private Integer lastCheckinExpiredAt;

    /**
     * 
     * @return
     *     The lastCheckinExpiredAt
     */
    public Integer getLastCheckinExpiredAt() {
        return lastCheckinExpiredAt;
    }

    /**
     * 
     * @param lastCheckinExpiredAt
     *     The lastCheckinExpiredAt
     */
    public void setLastCheckinExpiredAt(Integer lastCheckinExpiredAt) {
        this.lastCheckinExpiredAt = lastCheckinExpiredAt;
    }

}
