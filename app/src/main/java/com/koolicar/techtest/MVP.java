package com.koolicar.techtest;


import com.koolicar.techtest.common.ActivityView;
import com.koolicar.techtest.common.ModelOps;
import com.koolicar.techtest.common.PresenterOps;
import com.koolicar.techtest.model.venues.Venue;

import java.util.List;

/**
 * <p>
 * Interface that holds all operations available to MVP layers.
 * Controls the communication process between each layer
 * </p>
 *
 * <strong>Using</strong>
 * <code>interface RequiredViewOps extends ActivityView</code>
 * with VIEW methods to be accessed by PRESENTER
 *
 * <code>interface ProvidedPresenterOps extends PresenterOps<RequiredViewOps></code>
 * Operations offered to VIEW to communicate with PRESENTER
 *
 * <code>interface RequiredPresenterOps</code>
 * with Required PRESENTER methods available to MODEL
 *
 * <code>interface ProvidedModelOps extends ModelOps<RequiredPresenterOps></code>
 * Operations offered to MODEL to communicate with PRESENTER
 *
 * Created by: Tin Megali on 25/02/16. <br/>
 * Project: AndroidMVP </br>
 * --------------------------------------------------- <br />
 * <a href="http://www.tinmegali.com">tinmegali.com</a> <br/>
 * <a href="http://www.github.com/tinmegali>github</a> <br />
 * ---------------------------------------------------
 * <p>
 * Based on
 * <a href="https://github.com/douglascraigschmidt/POSA-15/tree/master/ex/AcronymExpander/src/vandy/mooc">
 * framework MVP</a> developed by
 * <a href="https://github.com/douglascraigschmidt">
 * Dr. Douglas Schmidth</a>
 * </p>
 *
 */

public interface MVP {

    /**
     * Required VIEW methods available to PRESENTER
     *      PRESENTER to VIEW
     */
    interface RequiredViewOps extends ActivityView {
        void displayVenueList(List<Venue> venueList);
        void displayVenueDetail(Venue venue);
    }


    /**
     * Operations offered to VIEW to communicate with PRESENTER
     *      VIEW to PRESENTER
     */
    interface ProvidedPresenterOps extends PresenterOps<RequiredViewOps> {
        void fetchVenueList();
        void downloadVenuePhoto(Venue venue);
    }

    /**
     * Required PRESENTER methods available to MODEL
     *      MODEL to PRESENTER
     */
    interface RequiredPresenterOps {
        void onListDownloaded(List<Venue> venueList);
        void onVenuePhotoDownloaded(Venue venue);
    }

    /**
     * Operations offered to MODEL to communicate with PRESENTER
     *      PRESENTER to MODEL
     */
    interface ProvidedModelOps extends ModelOps<RequiredPresenterOps> {
        void downloadVenuesList();
        void downloadVenueImage(Venue venue);
    }

}
