package com.koolicar.techtest.presenter;

import com.koolicar.techtest.MVP;
import com.koolicar.techtest.common.GenericPresenter;
import com.koolicar.techtest.model.VenuesModel;
import com.koolicar.techtest.model.venues.Venue;

import java.util.List;

/**
 * Created by djameldrifel on 2016-12-10.
 */

public class MainPresenter extends GenericPresenter<MVP.RequiredPresenterOps, MVP.ProvidedModelOps, MVP.RequiredViewOps, VenuesModel>
        implements MVP.RequiredPresenterOps, MVP.ProvidedPresenterOps {

    @Override
    public void onCreate(MVP.RequiredViewOps view) {
        super.onCreate(VenuesModel.class, this);
        setView(view);
    }

    @Override
    public void onConfigurationChanged(MVP.RequiredViewOps view) {
        setView(view);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void fetchVenueList() {
        getModel().downloadVenuesList();
    }

    @Override
    public void downloadVenuePhoto(Venue venue) {
        getModel().downloadVenueImage(venue);
    }

    @Override
    public void onListDownloaded(List<Venue> venueList) {
        getView().displayVenueList(venueList);
    }

    @Override
    public void onVenuePhotoDownloaded(Venue venue) {
        getView().displayVenueDetail(venue);
    }
}
